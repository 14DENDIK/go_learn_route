package main

import (
  "log"

  "golang.org/x/net/context"
  "google.golang.org/grpc"

  "github.com/tutorialedge/go-grpc-tutorial/chat"
)

func main() {
  var conn *grpc.ClientConn
  conn, err := grpc.Dial(":9000", grpc.WithInsecure())
  if err != nil {
    log.Fatalf("Could not connect on port 9000: %v", err)
  }
  defer conn.Close()

  c := chat.NewChatServiceClient(conn)

  message := chat.Message {
    Body: "Message From the Client",
  }

  response, err := c.SayHello(context.Background(), &message)
  if err != nil {
    log.Fatalf("Say hello could not be called: %v", err)
  }

  log.Printf("Response from server: %s", response.Body)
}
