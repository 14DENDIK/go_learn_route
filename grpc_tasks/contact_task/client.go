package main

import (
  "log"

  "golang.org/x/net/context"
  "google.golang.org/grpc"
  gpb "github.com/golang/protobuf/ptypes/empty"
  "bitbucket.org/14DENDIK/firstapp/grpc_tasks/contact_task/contact"
)

func main() {
  var conn *grpc.ClientConn
  conn, err := grpc.Dial(":9000", grpc.WithInsecure())
  if err != nil {
    log.Fatalf("Could not connect on port 9000: %v", err)
  }
  defer conn.Close()

  c := contact.NewContactServiceClient(conn)

  response, err := c.GetContacts(context.Background(), &gpb.Empty{})
  if err != nil {
    log.Fatalf("Did not get contacts: %v", err)
  }
  // log.Println(response.Contacts)
  for _, v := range(response.Contacts) {
    log.Println(v)
  }
}
