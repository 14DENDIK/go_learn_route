package main

import (
  "net"
  "log"

  "google.golang.org/grpc"
  "bitbucket.org/14DENDIK/firstapp/grpc_tasks/contact_task/contact"
)

func main() {
  lis, err := net.Listen("tcp", ":9000")
  if err != nil {
    log.Fatalf("The port number 9000 is busy: %v", err)
  }

  s := contact.Server{}

  grpcServer := grpc.NewServer()

  contact.RegisterContactServiceServer(grpcServer, &s)

  if err := grpcServer.Serve(lis); err != nil {
    log.Fatalf("Failed to serve gRPC: %v", err)
  }
}
