package contact

import (
  "fmt"
  "log"
  "time"
  "database/sql"
  _ "github.com/lib/pq"
  "golang.org/x/net/context"
  gpb "github.com/golang/protobuf/ptypes/empty"
)

const (
  host     = "localhost"
	port     = 5432
	user     = "sardor"
	password = "1"
	dbname   = "gopsql"
)

var (
  db *sql.DB
  err error
  psqlInfo string = fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable",
    host, port, user, password, dbname)
)

type Server struct {}

func Initializer() {
  db, err = sql.Open("postgres", psqlInfo)
  if err != nil {
    log.Fatalf("Could not connect to database: %v", err)
  }
}

func (s *Server) AddContact(ctx context.Context, c *Contact) (*Contact, error) {
  Initializer()
  defer db.Close()
  err = db.QueryRowContext(ctx, "INSERT INTO contacts(name, phone_num, created_at) VALUES($1, $2, $3) returning id;", c.Name, c.PhoneNum, c.CreatedAt).Scan(&c.Id)
  if err != nil {
    log.Fatalf("Could not add to a database: %v", err)
  }
  return c, nil
}

func (s *Server) UpdateContact(ctx context.Context, c *Contact) (*Contact, error) {
  Initializer()
  defer db.Close()
  stmt, err := db.PrepareContext(ctx, "UPDATE contacts SET name=$1, phone_num=$2, created_at=$3 WHERE id=$4;")
  if err != nil {
    log.Fatalf("Error in preporation process: %v", err)
  }
  res, err := stmt.ExecContext(ctx, c.Name, c.PhoneNum, time.Now().String(), c.Id)
  if err != nil {
    log.Fatalf("Could not update contact: %v", err)
  }
  _, err = res.RowsAffected()
  if err != nil {
    log.Fatalf("No rows are affected!: %v", err)
  }
  return c, nil
}

func (s *Server) DeleteContact(ctx context.Context, c *ContactId) (*Status, error) {
  Initializer()
  defer db.Close()
  stmt, err := db.PrepareContext(ctx, "DELETE FROM contacts WHERE id=$1;")
  if err != nil {
    log.Fatalf("Statement error: %v", err)
  }
  res, err := stmt.ExecContext(ctx, c.Id)
  if err != nil {
    log.Fatalf("Could not execute: %v", err)
  }
  affect, err := res.RowsAffected()
  if err != nil {
    log.Fatalf("No rows affected: %v", err)
  }
  if affect == 1 {
    return &Status{Status: "Deleted"}, nil
  }
  return nil, err
}

func (s *Server) GetContacts(ctx context.Context,a *gpb.Empty) (*GetContactsResponse, error) {
  Initializer()
  defer db.Close()
  var id int64
  var name string
  var phoneNum string
  var createdAt string
  var contacts []*Contact
  rows, err := db.QueryContext(ctx, "SELECT * FROM contacts;")
  if err != nil {
    log.Fatalf("Error in getting from database: %v", err)
  }
  for rows.Next() {
    err = rows.Scan(&id, &name, &phoneNum, &createdAt)
    if err != nil {
      log.Fatalf("Could not get a row!")
    }
    contacts = append(contacts, &Contact{Id: id, Name:name, PhoneNum:phoneNum, CreatedAt:createdAt})
  }
  return &GetContactsResponse{Contacts: contacts}, nil
}
