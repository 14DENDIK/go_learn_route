package main

import (
  "log"
  "time"
  "golang.org/x/net/context"
  "google.golang.org/grpc"
  "bitbucket.org/14DENDIK/firstapp/grpc_tasks/todo_task/todo"
)

func main() {
  var conn *grpc.ClientConn
  conn, err := grpc.Dial(":9000", grpc.WithInsecure())
  if err != nil {
    log.Fatalf("Could not connect on port 9000: %v", err)
  }
  defer conn.Close()

  c := todo.NewTodoServiceClient(conn)

  status, err := c.DeleteTodo(context.Background(), &todo.DeleteTodoRequest{Id: 66})
  if err != nil {
    log.Fatalf("Did not get todos: %v", err)
  }
  log.Println(status.Status)

  res, err := c.AddTodo(context.Background(), &todo.Todo{Task: "Test grpc", Finished: true, CreatedAt: time.Now().Format("01-02-2006")})
  if err != nil {
    log.Fatalf("Did not added anything: %v", err)
  }
  log.Println(res)

  updatedTodo, err := c.UpdateTodo(context.Background(), &todo.UpdateTodoRequest{Id: 60, Task: "Run updated", Finished: true, CreatedAt: time.Now().Format("01-02-2006")})
  if err != nil {
    log.Fatalf("Could not update todo: %v", err)
  }
  log.Println(updatedTodo)

  response, err := c.GetTodos(context.Background(), &todo.Empty{})
  for _, v := range(response.Todos) {
    log.Println(v)
  }
}
