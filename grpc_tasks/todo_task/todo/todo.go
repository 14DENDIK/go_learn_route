package todo

import (
	"fmt"
	"log"
  "database/sql"
	_ "github.com/lib/pq"
	"golang.org/x/net/context"
)

type Server struct {}

const (
	host     = "localhost"
	  port     = 5432
	  user     = "sardor"
	  password = "1"
	  dbname   = "gopsql"
  )

  var (
	db *sql.DB
	err error
	psqlInfo string = fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable",
	  host, port, user, password, dbname)
  )


func initializer() {
	db, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Fatalf("Could not open a database: %v\n", err)
	}
}


func (s *Server) AddTodo(ctx context.Context, t *Todo) (*Empty, error) {
	initializer()
	defer db.Close()

	err = db.QueryRowContext(ctx, "INSERT INTO todos(task, finished, created_at) VALUES($1, $2, $3) returning id;", t.Task, t.Finished, t.CreatedAt).Scan(&t.Id)
	if err != nil {
		log.Fatalf("Could not insert to databse: %v", err)
	}
	return &Empty{}, nil
}

func (s *Server) UpdateTodo(ctx context.Context, utr *UpdateTodoRequest) (*Todo, error) {
	initializer()
	defer db.Close()
	stmt, err := db.PrepareContext(ctx, "UPDATE todos SET task=$1, finished=$2, created_at=$3 WHERE id=$4;")
	if err != nil {
		log.Fatalf("Update could not be handled: %v", err)
	}
	res, err := stmt.ExecContext(ctx, utr.Task, utr.Finished, utr.CreatedAt, utr.Id)
	if err != nil {
		log.Fatalf("Cant execute the statement: %v", err)
	}
	_, err = res.RowsAffected()
	if err != nil {
		log.Fatalf("No rows affected: %v", err)
	}
	return &Todo{Id: utr.Id, Task: utr.Task, Finished: utr.Finished, CreatedAt: utr.CreatedAt}, nil
}

func (s *Server) DeleteTodo(ctx context.Context, dtr *DeleteTodoRequest) (*DeleteTodoResponse, error) {
	initializer()
	defer db.Close()

	stmt, err := db.PrepareContext(ctx, "DELETE FROM todos WHERE id=$1;")
	if err != nil {
		log.Fatalf("Error in preperation: %v", err)
	}
	res, err := stmt.ExecContext(ctx, dtr.Id)
	if err != nil {
		log.Fatalf("Could not delete: %v", err)
	}
	_, err = res.RowsAffected()
	if err != nil {
		log.Fatalf("Row not affected: %v", err)
	}
	return &DeleteTodoResponse{Status: "Deleted"}, nil
}

func (s *Server) GetTodos(ctx context.Context, e *Empty) (*GetTodosResponse, error) {
	initializer()
	defer db.Close()

	var id int64
  var task string
  var finished bool
  var createdAt string
	var todos []*Todo
	rows, err := db.QueryContext(ctx, "SELECT * FROM todos;")
	if err != nil {
		log.Fatalf("Could not get anything: %v", err)
	}
	for rows.Next() {
		err = rows.Scan(&id, &task, &finished, &createdAt)
		if err != nil {
			log.Fatalf("Error when getting from database: %v", err)
		}
		todos = append(todos, &Todo{Id: id, Task: task, Finished: finished, CreatedAt: createdAt})
	}
	return &GetTodosResponse{Todos: todos}, nil
}
