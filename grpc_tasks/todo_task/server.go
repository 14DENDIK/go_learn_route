package main

import (
  "net"
  "log"

  "google.golang.org/grpc"
  "bitbucket.org/14DENDIK/firstapp/grpc_tasks/todo_task/todo"
)

func main() {
  lis, err := net.Listen("tcp", ":9000")
  if err != nil {
    log.Fatalf("The port number 9000 is busy: %v", err)
  }

  s := todo.Server{}

  grpcServer := grpc.NewServer()

  todo.RegisterTodoServiceServer(grpcServer, &s)

  if err := grpcServer.Serve(lis); err != nil {
    log.Fatalf("Failed to serve gRPC: %v", err)
  }
}
